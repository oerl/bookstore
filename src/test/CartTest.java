package test;

import java.math.BigDecimal;

import org.junit.*;
import program.Book;
import program.Cart;

public class CartTest {
private Book book;
private Cart cart;
	
	@Before
	public void setUp(){
		cart=new Cart();
		book=new Book("Title","Author",new BigDecimal(5.0));
	}
	
	@Test
	public void addbookToCart(){
		cart.addBook(book);
		Assert.assertTrue(cart.getShoppingCart().size()==1);
	}
	
	@Test
	public void removeBookFromCart(){
		cart.addBook(book);
		cart.addBook(book);
		cart.addBook(book);
		cart.remove(1);
		Assert.assertTrue(cart.getShoppingCart().size()==2);
	}
	
	@Test
	public void removeAllBooksFromCart(){
		cart.addBook(book);
		cart.addBook(book);
		cart.addBook(book);
		cart.removeAll();
		Assert.assertTrue(cart.getShoppingCart().size()==0);
	}
}
