package test;

import java.math.BigDecimal;


import org.junit.*;

import program.Book;
import program.BookListImpl;
import program.BookParser;

public class BookListImplTest {

	BookParser parser;
	BookListImpl stock;

	@Before
	public void setUp() {
		parser = new BookParser();
		stock = new BookListImpl();
		parser.parseTxtFile(stock.getAvailableBooks(), "/path/to/textfile"); // path to text file
	}

	@Test
	public void initiialList() {
		Book[] booksInStock = stock.list("");
		Assert.assertTrue(booksInStock.length > 0);
	}
	
	@Test
	public void searchForBook(){
		Book[]booksResult=stock.list("First Author");
		Assert.assertTrue(booksResult.length==1);
	}
	
	@Test
	public void addBook(){
		int countBeforeAdd=stock.getAvailableBooks().size();
		stock.add(new Book("Hello","Author",new BigDecimal("1.50")), 5);
		Assert.assertTrue(stock.getAvailableBooks().size()==countBeforeAdd+1);
	}
	
	@Test
	public void boughtBooksStatus(){
		Book[] cart=new Book[2];
		Book bookA=new Book(stock.getAvailableBooks().get(0));
		bookA.setQuantity(55);
		cart[0]=bookA;
		bookA=new Book(stock.getAvailableBooks().get(2));
		bookA.setQuantity(2);
		cart[1]=bookA;
		int status[]=stock.buy(cart);
		Assert.assertArrayEquals(status,new int[]{1,0} );
		
	}

}
