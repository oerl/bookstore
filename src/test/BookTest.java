package test;

import java.math.BigDecimal;

import org.junit.*;

import program.*;

public class BookTest {
	private Book book;
	
	@Before
	public void setUp(){
		book=new Book("Title","Author",new BigDecimal(5.0));
	}
	
	@Test
	public void createBook(){
		
		Assert.assertEquals(book.getAuthor(), "Author");
		Assert.assertEquals(book.getTitle(),"Title");
		Assert.assertEquals(book.getPrice(), new BigDecimal(5.0));
	}
	
	@Test
	public void creatBookFromBook(){
		Book bookB=new Book(book);
		Assert.assertEquals(bookB.getAuthor(), "Author");
		Assert.assertEquals(bookB.getTitle(),"Title");
		Assert.assertEquals(bookB.getPrice(), new BigDecimal(5.0));
	}
	
	@Test
	public void addQuantity(){
		book.addQuantity(5);
		Assert.assertEquals(book.getQuantity(), 5);
	}
	
	@Test
	public void modifyQuantity(){
		book.addQuantity(5);
		book.addQuantity(-2);
		Assert.assertEquals(book.getQuantity(), 3);
	}

}
