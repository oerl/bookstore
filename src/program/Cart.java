package program;

import java.util.ArrayList;

/**
 * @author Omar
 *
 */
public class Cart {

	/**
	 * ArrayList to store the books in the shopping cart
	 */
	private ArrayList<Book> shoppingCart = new ArrayList<Book>(); 

	/**
	 * Add book to the shopping cart
	 * 
	 * @param book
	 */
	public void addBook(Book book) {
		shoppingCart.add(book);
	}

	/**
	 * @return
	 */
	public ArrayList<Book> getShoppingCart() {
		return shoppingCart;
	}

	/**
	 * Remove a book at a specified index "i"
	 * @param i
	 */
	public void remove(int i) {
		shoppingCart.remove(i);
	}

	/**
	 * Display the shopping cart with prices per book
	 * @param booksInCart
	 */
	public void itemsWithPrice(Book[] booksInCart) {

		System.out.println("Current Shopping Cart: ");
		System.out.println(
				"+---------------------------------------------------------------------------------------------------------------+");

		System.out.format("%10s%20s%20s%20s%20s%20s\n", "Number", "Title", "Author", "Price", "Quantity",
				"Price/Quantity");
		double totalPrice = 0.0;

		for (int i = 0; i < booksInCart.length; i++) {
			totalPrice += booksInCart[i].getQuantity() * booksInCart[i].getPrice().doubleValue();
			System.out.format("%10s", i + 1);
			System.out.format("%20s", booksInCart[i].getTitle());
			System.out.format("%20s ", booksInCart[i].getAuthor());
			System.out.format("%20.2f ", booksInCart[i].getPrice().doubleValue());
			System.out.format("%20d ", booksInCart[i].getQuantity());
			System.out.format("%20.2f ", (booksInCart[i].getQuantity() * booksInCart[i].getPrice().doubleValue()));
			System.out.println();
		}
		System.out.format("%105s%10.2f", "TotalPrice ", totalPrice);
		System.out.println();
		System.out.println(
				"+---------------------------------------------------------------------------------------------------------------+");
		

	}
	
	/**
	 * Remove all the elements in the Shopping Cart
	 */
	public void removeAll(){
		shoppingCart.removeAll(shoppingCart);
	}

//	
	/**
	 * Display the shopping cart with the status of each book 
	 * @param booksInCart
	 * @param booksStatus
	 */
	public void itemsAfterBuying(Book[] booksInCart, int[] booksStatus) {
		System.out.format("%10s%20s%20s%20s%20s%20s\n", "Number", "Title", "Author", "Price", "Quantity", "Status");

		for (int i = 0; i < booksInCart.length; i++) {

			System.out.format("%10s", i + 1);
			System.out.format("%20s", booksInCart[i].getTitle());
			System.out.format("%20s ", booksInCart[i].getAuthor());
			System.out.format("%20.2f ", booksInCart[i].getPrice().doubleValue());
			System.out.format("%20d ", booksInCart[i].getQuantity());
			if (booksStatus[i] == 0) {

				System.out.format("%20s ", "OK");

			} else if (booksStatus[i] == 1) {
				shoppingCart.remove(i);
				System.out.format("%20s ", "NOT_IN_STOCK (Removed)");
			} else {
				shoppingCart.remove(i);
				System.out.format("%20s ", "DOES_NOT_EXIST (Removed)");
			}

			System.out.println();
		}

	}

}
