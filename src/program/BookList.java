package program;

public interface BookList {
	
	/**
	 * Display all the books in the stock or display a selected book
	 * @param searchString
	 * @return
	 */
	public Book[] list(String searchString);

	/**
	 * Add a book to the stock with a specified quantity
	 * @param book
	 * @param quantity
	 * @return
	 */
	public boolean add(Book book, int quantity);

	/**
	 * Check the status of the bought books with the books in stock
	 * @param books
	 * @return
	 */
	public int[] buy(Book... books);
}