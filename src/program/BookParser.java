package program;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Locale;

import javax.swing.JFileChooser;


/**
 * @author Omar
 *
 */
public class BookParser {
	/**
	 * @param books
	 */
	public void parseTxtFile(ArrayList<Book> books, String name) {

		String fileName = "";

		if (name.length() == 0) {
			JFileChooser fileChooser = new JFileChooser();
			int status = fileChooser.showOpenDialog(null);
			File selectedFile;
			if (status == JFileChooser.APPROVE_OPTION) {
				selectedFile = fileChooser.getSelectedFile();
				fileName = selectedFile.getParent() + "/" + selectedFile.getName(); // path
																					// to
																					// text
																					// file
			} else if (status == JFileChooser.CANCEL_OPTION) {
				System.out.println("canceled");
				System.exit(1);
			}

		} else {
			fileName = name;
		}

		// This will reference one line at a time
		String line = null;

		try {
			// FileReader reads text files in the default encoding.
			FileReader fileReader = new FileReader(fileName);

			// Always wrap FileReader in BufferedReader.
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			while ((line = bufferedReader.readLine()) != null) {
				String[] matches = line.split(";");
				for (int i = 0; i < matches.length; i += 4) {
					NumberFormat formatter = NumberFormat.getNumberInstance(Locale.ENGLISH);

					try {

						Number number = formatter.parse(matches[i + 2]);
						BigDecimal price = BigDecimal.valueOf(number.doubleValue());
						Book book = new Book(matches[i], matches[i + 1], price);
						book.addQuantity(Integer.valueOf(matches[i + 3]));
						books.add(book);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}

			// Always close files.
			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
			
		} catch (IOException ex) {
			ex.printStackTrace();
			
		}
	}
}
