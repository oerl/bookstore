package program;
import java.util.ArrayList;

/**
 * @author Omar
 *
 */
public class BookListImpl implements BookList {
	
	ArrayList<Book> availableBooks = new ArrayList<Book>();

	@Override
	public Book[] list(String searchString) {
		ArrayList<Book> bookList = new ArrayList<Book>();

		if (searchString.length() == 0) {
			return availableBooks.toArray(new Book[availableBooks.size()]);
		} else {
			for (Book b : availableBooks) {
				if (b.getTitle().toLowerCase().contains(searchString.toLowerCase())
						|| b.getAuthor().toLowerCase().contains(searchString.toLowerCase()))
					bookList.add(b);
			}
		}
		return bookList.toArray(new Book[bookList.size()]);
	}

	@Override
	public boolean add(Book book, int quantity) {
		book.addQuantity(quantity);
		availableBooks.add(book);
		return true;
	}

	@Override
	public int[] buy(Book... books) {
		int[] status = new int[books.length];
		for (int i = 0; i < books.length; i++) {
			for (int j = 0; j < availableBooks.size(); j++) {
				if (availableBooks.get(j).compareBooks(books[i])) {
					if (availableBooks.get(j).getQuantity() >= books[i].getQuantity()) {
						status[i] = 0;
						break;
					} else {
						status[i] = 1;
						break;
					}
				}
				status[i] = 2;
			}
		}
		return status;
	}

	/**
	 * Print the books in stock with their quantities
	 * @param booksInStock
	 */
	public void displayBooksInStock(Book[] booksInStock) {
		if (booksInStock.length > 0) {
			System.out.format("%10s%20s%20s%20s%20s\n", "Number", "Title", "Author", "Price", "Quantity");
			int index = 1;
			for (final Book row : booksInStock) {
				System.out.format("%10s", index);
				System.out.format("%20s", row.getTitle());
				System.out.format("%20s ", row.getAuthor());
				System.out.format("%20.2f ", row.getPrice().doubleValue());
				System.out.format("%20d ", row.getQuantity());
				index++;
				System.out.println();
			}
		} else {
			System.out.println("Sorry no books found");
			System.exit(1);
		}

	}

	/**
	 * Modify the quantity of the book after being purchased
	 * @param booksInCart
	 */
	public void modifyQuantityAfterPurchase(Book[] booksInCart) {
		for (int i = 0; i < booksInCart.length; i++) {
			for (int j = 0; j < availableBooks.size(); j++) {
				if (availableBooks.get(j).compareBooks(booksInCart[i])) {
					availableBooks.get(j).addQuantity((-1*booksInCart[i].getQuantity()));
				}
				

			}
		}
	}

	public ArrayList<Book> getAvailableBooks() {
		return availableBooks;
	}

	public void setAvailableBooks(ArrayList<Book> availableBooks) {
		this.availableBooks = availableBooks;
	}
}
