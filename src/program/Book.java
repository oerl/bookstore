package program;

import java.math.BigDecimal;

/**
 * @author Omar
 *
 */
public class Book {

	private String title;
	private String author;
	private BigDecimal price;
	private int quantity;

	public Book(String title, String author, BigDecimal price) {
		this.title = title;
		this.author = author;
		this.price = price;
	}

	public Book(Book book) {
		this(book.title, book.author, book.price);
	}

	public boolean compareBooks(Book b) {
		if (this.getAuthor().equals(b.getAuthor()) && (this.getPrice() == (b.getPrice()))
				&& (this.getTitle().equals(b.getTitle()))) {
			return true;
		}
		return false;

	}

	public void addQuantity(int amount) {
		quantity += amount;

	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
