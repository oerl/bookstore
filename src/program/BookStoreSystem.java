package program;

import java.math.BigDecimal;
import java.util.Locale;
import java.util.Scanner;

/**
 * @author Omar
 *
 */
public class BookStoreSystem {

	public static void main(String[] args) {

		BookListImpl stock = new BookListImpl();
		BookParser parser = new BookParser();
		Cart shopingCart = new Cart();
		Book[] booksInStock = new Book[] {};
		Scanner scan = new Scanner(System.in);
		if (args.length > 0)
			parser.parseTxtFile(stock.availableBooks, args[0]);
		else
			parser.parseTxtFile(stock.availableBooks, "");
		while (true) {
			System.out.println("+----------------------------------+");
			System.out.println("|  Welcome to the Amazing Library  |");
			System.out.println("+----------------------------------+");
			System.out.println("|Please choose an option :         |");
			System.out.println("|1.Display all books               |");
			System.out.println("|2.Search by Title/Author          |");
			System.out.println("|3.Add Book to stock               |");
			System.out.println("|4.Exit                            |");
			System.out.println("+----------------------------------+");
			System.out.println("Option :");
			
			scan.useLocale(Locale.ENGLISH);
			int option = Integer.valueOf(scan.nextLine());

			if (option == 1) {
				booksInStock = stock.list("");// get all the books in the stock
				stock.displayBooksInStock(booksInStock);
			} else if (option == 2) {
				System.out.println("Please enter Title or Author: ");
				String title = scan.nextLine();
				booksInStock = stock.list(title.toLowerCase());// get all the
																// books match
																// the title or
																// author
																// entered
				stock.displayBooksInStock(booksInStock);
			} else if (option == 3) {
				booksInStock = stock.list("");// get all the books in the stock
												// efore adding
				stock.displayBooksInStock(booksInStock);
				System.out.println("Enter Book Title");
				String title = scan.nextLine();
				System.out.println("Enter Book Author");
				String author = scan.nextLine();
				System.out.println("Enter Book Price");
				Double price = scan.nextDouble();
				scan.nextLine();
				System.out.println("Enter Book Quantity");
				int quantity = Integer.valueOf(scan.nextLine());
				Book book = new Book(title, author, new BigDecimal(price));
				stock.add(book, quantity);
				System.out.println("Stock after adding \n\n");
				booksInStock = stock.list("");// get all the books in the stock
												// after adding
				stock.displayBooksInStock(booksInStock);

			} else if (option == 4) {
				System.out.println("Good Bye !");
				scan.close();
				System.exit(1);
			}
			while (true) {

				Book[] booksInCart = shopingCart.getShoppingCart()
						.toArray(new Book[shopingCart.getShoppingCart().size()]);
				int booksStatus[] = stock
						.buy(shopingCart.getShoppingCart().toArray(new Book[shopingCart.getShoppingCart().size()]));
				System.out.println("Would you like to add/delete a book to shopping cart or checkout ?");
				System.out.println("Choice: ");
				String choice = scan.nextLine();
				if (choice.equals("add")) {
					// add book to the shopping cart
					System.out.println("Please enter book number");
					int bookNumber = Integer.valueOf(scan.nextLine());
					System.out.println("Enter Quantity");
					int quantity = Integer.valueOf(scan.nextLine());
					if (bookNumber < booksInStock.length || bookNumber > 1) { // validate
																				// user
																				// input
						Book book = new Book(booksInStock[bookNumber - 1]);
						book.setQuantity(quantity);
						shopingCart.addBook(book);
					}

				} else if (choice.toLowerCase().equals("checkout") && shopingCart.getShoppingCart().size() == 0) {
					System.out.println("Shoping cart is empty");
					break;

				} else if (choice.toLowerCase().equals("checkout") && shopingCart.getShoppingCart().size() > 0) {

					if (booksInCart.length > 0) {
						shopingCart.itemsAfterBuying(booksInCart, booksStatus);// check
																				// the
																				// status
																				// of
																				// selected
																				// books
						if (shopingCart.getShoppingCart().size() > 0) {
							booksInCart = shopingCart.getShoppingCart()
									.toArray(new Book[shopingCart.getShoppingCart().size()]);
							stock.modifyQuantityAfterPurchase(booksInCart); // change
																			// the
																			// quantity
																			// of
																			// the
																			// books
																			// after
																			// purchase
							shopingCart.itemsWithPrice(booksInCart); // display
																		// the
																		// final
																		// shopping
																		// cart
							shopingCart.removeAll();
							System.out.println("Do you want to buy again ? (Y/N)");
							System.out.print("Choice:");
							String buyChoiceAfterPurchase = scan.nextLine();
							if (buyChoiceAfterPurchase.equals("y"))
								break;
							else if (buyChoiceAfterPurchase.equals("n")) {
								System.out.println("Thanks for purchasing");
								scan.close();
								System.exit(1);
							}
						}
					}
					break;
				} else if (choice.toLowerCase().equals("delete")) {
					
					shopingCart.itemsAfterBuying(booksInCart, booksStatus); // check
																			// the
																			// status
																			// of
																			// the
																			// added
																			// books

					booksInCart = shopingCart.getShoppingCart().toArray(new Book[shopingCart.getShoppingCart().size()]);
					if (booksInCart.length > 0) {
						shopingCart.itemsWithPrice(booksInCart);// display the
																// shopping cart
																// after
																// checking

						System.out.println("Enter book number to be removed");
						System.out.print("Book Number: ");
						int bookNumberToRemove = Integer.valueOf(scan.nextLine());
						if (bookNumberToRemove > 0 && bookNumberToRemove < shopingCart.getShoppingCart().size() + 1) // verify
																														// book
																														// number
																														// to
																														// remove
							shopingCart.remove(bookNumberToRemove - 1);

					} else {// if user choose to remove and cart is empty
						System.out.println("Shoping cart is empty");
						break;
					}
				}
			}
			
		}
		
	}
}
